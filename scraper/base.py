from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import os


class Chrome():

    def __init__(self, browser="chrome", options=None):
        self.driver_options = Options()
        self.driver_options.add_argument("--no-sandbox")
        self.driver_options.add_argument("--headless")
        self.driver_options.add_argument("--start-maximized")

        if options is not None:
            if isinstance(options, basestring):
                options = options.split(" ")

            for args in options:
                self.driver_options.add_argument(args)

    def create(self):
        return webdriver.Chrome(
            executable_path=os.environ['CHROME_WEBDRIVER'],
            options=self.driver_options
        )
