import argparse
import pandas as pd
import os

from dateparser import parse
from datetime import datetime
from scaleway.s3 import read_file
from sqlalchemy import create_engine

parser = argparse.ArgumentParser()
parser.add_argument('--table', help='table name', default=None)
parser.add_argument('--dt', help='dt', default=None)

(args, unknown) = parser.parse_known_args()
table_name = args.table
dt = parse(args.dt)

db_pass = os.environ['PG_PASS']

table_dict = {
    "player_count": {
        "s3": "hourly.game.playerCount",
        "pg": "hourly_player_count"
    },
    "game_detail": {
        "s3": "hourly.game.gameDetail",
        "pg": "hourly_game_detail"
    }
}


def main():
    data = read_file(table_dict[table_name]["s3"], dt)
    df = pd.DataFrame(data)

    if table_name == 'player_count':
        df['id'] = df['link'].str.rsplit('/', 1)
        df['id'] = df['id'].apply(lambda x: x[-1])
        df['timestamp'] = df['timestamp'].apply(
            lambda x: datetime.fromtimestamp(x)
        )

        df = df.rename(columns={"timestamp": "datetime"})
    elif table_name == "game_detail":
        pass

    dialect = (
        'postgresql://postgres:{0}@localhost:5432/steam'
        .format(
            db_pass
        )
    )
    engine = create_engine(dialect)
    df.to_sql(
        table_dict[table_name]['pg'],
        engine,
        index=False,
        if_exists='append')


if __name__ == "__main__":
    main()
