from scraper.base import Chrome
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from itertools import izip_longest
from datetime import datetime
from scaleway.s3 import write_file
from time import mktime


now = datetime.utcnow()


def scrape(page, timestamp):

    xpath = {
        "app_id": '//*[@id="top-games"]/tbody/tr/td/a',
        "game_title": '//*[@id="top-games"]/tbody/tr/td/a/',
        "player_count": '//*[@id="top-games"]/tbody/tr/td[3]'
    }

    base_link = (
        "https://steamcharts.com/top/p.{0}"
        .format(str(page))
    )

    browser = Chrome().create()
    browser.get(base_link)
    delay = 5  # seconds

    # Wait JS load
    wait = WebDriverWait(browser, delay)
    wait.until(
        EC.presence_of_element_located(
            (By.XPATH, xpath['app_id'])
        )
    )

    title_link = browser.find_elements_by_xpath(
        '//*[@id="top-games"]/tbody/tr/td[2]/a'
    )

    player_count = browser.find_elements_by_xpath(
        '//*[@id="top-games"]/tbody/tr/td[3]'
    )

    data = []

    # Extract data
    for title_element, count in izip_longest(title_link, player_count):
        new_data = dict()
        new_data['title'] = title_element.text
        new_data['player_count'] = int(count.text.replace(',', ''))
        new_data['link'] = title_element.get_attribute('href')
        new_data['id'] = int(new_data.get('link').rsplit('/', 1)[-1])
        new_data['timestamp'] = mktime(now.timetuple())
        data.append(new_data)

    browser.quit()
    return data


def main():
    top_100_page = [i for i in range(1, 5)]
    all_data = []

    # Get popular game data
    for i in top_100_page:
        for data in scrape(i, now):
            all_data.append(data)

    # Write to S3
    write_file(all_data, "hourly.game.playerCount", now)


if __name__ == '__main__':
    main()
