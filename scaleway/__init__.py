import os
import boto3

SCW_ACCESS_KEY_ID = os.environ['SCW_ACCESS_KEY_ID']
SCW_SECRET_ACCESS_KEY = os.environ['SCW_SECRET_ACCESS_KEY']
SCW_ENDPOINT = 'https://s3.fr-par.scw.cloud'
SCW_REGION = 'fr-par'
session = boto3.Session(region_name=SCW_REGION)


def s3_connect():
    return session.resource(
        "s3",
        endpoint_url=SCW_ENDPOINT,
        aws_access_key_id=SCW_ACCESS_KEY_ID,
        aws_secret_access_key=SCW_SECRET_ACCESS_KEY
    )


def s3_client():
    return session.client(
        "s3",
        endpoint_url=SCW_ENDPOINT,
        aws_access_key_id=SCW_ACCESS_KEY_ID,
        aws_secret_access_key=SCW_SECRET_ACCESS_KEY
    )
