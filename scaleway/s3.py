from scaleway import s3_connect
import json


def generate_dir(filename, timestamp):
    directory = (
        "data/scraping/"
        "{0}/{1}/{2}.json"
        .format(
            filename,
            timestamp.strftime("%Y-%m-%d"),
            timestamp.strftime("%H")
        )
    )

    path = {
        "bucket": "narrow-toilet",
        "path": directory
    }

    return path


def write_file(data, filename, timestamp):
    directory = generate_dir(filename, timestamp)
    s3 = s3_connect()
    s3_object = s3.Object(
        directory['bucket'], directory['path']
    )
    s3_object.put(Body=json.dumps(data))


def read_file(filename, timestamp):
    directory = generate_dir(filename, timestamp)

    s3 = s3_connect()
    obj = s3.Object(directory['bucket'], directory['path'])
    return json.loads(obj.get()['Body'].read())
