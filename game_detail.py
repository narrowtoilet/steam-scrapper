from scaleway.s3 import read_file, write_file
from dateparser import parse

import requests
import argparse
import time

parser = argparse.ArgumentParser()
parser.add_argument('--dt', help='dt', default=None)
(args, unknown) = parser.parse_known_args()

now = parse(args.dt)
filename = 'hourly.game.playerCount'
data = read_file(filename, now)


def remove_non_ascii(text):
    try:
        return ''.join(i for i in text if ord(i) < 128)
    except UnicodeEncodeError:
        return text


def get_game_detail(parse_data=data):

    dim_data = []

    # Get game data
    for game in parse_data:
        baselink = "https://store.steampowered.com/api/appdetails?appids="
        id = game['link'].rsplit('/', 1)[-1]
        game_link = baselink + id

        _data = requests.get(game_link).json()

        new_data = dict()

        if _data[str(id)]['success'] is False:
            continue

        data = _data[str(id)]['data']

        new_data['id'] = int(id)

        new_data['type'] = str(data.get('type'))
        new_data['name'] = remove_non_ascii(data.get('name'))
        new_data['is_free'] = data.get('is_free')
        new_data['windows'] = data.get('platforms').get('windows')
        new_data['mac'] = data.get('platforms').get('mac')
        new_data['linux'] = data.get('platforms').get('linux')

        if data.get('metacritic'):
            new_data['metacritic_score'] = int(
                data.get('metacritic').get('score')
            )
        else:
            new_data['metacritic_score'] = None

        new_data['publishers'] = data.get('publishers')
        new_data['developers'] = data.get('developers')

        if data.get("price_overview"):
            price_overview = data.get("price_overview")
            new_data['currency'] = price_overview.get('currency')
            new_data['initial_price'] = price_overview.get('initial')
            new_data['discount'] = price_overview.get('discount_percent')
            new_data['selling_price'] = price_overview.get('final')
        else:
            new_data['currency'] = new_data['initial_price'] =\
                new_data['discount'] = new_data['selling_price'] = None

        if len(data['genres']) > 0:
            genre_list = []
            for i in data['genres']:
                genre_list.append(
                    str(i.get('description'))
                )
            new_data['genres'] = genre_list

        if data.get('dlc'):
            new_data['has_dlc'] = True if len(data['dlc']) > 0 else False
            new_data['dlc_count'] = len(data['dlc'])
        else:
            new_data['has_dlc'] = False
            new_data['dlc_count'] = None

        if data.get('packages'):
            new_data['in_package'] = True if len(
                data['packages']) > 0 else False
            new_data['packages_count'] = len(data['packages'])
        else:
            new_data['in_package'] = False
            new_data['packages_count'] = 0

        if data.get('recommendations'):
            if data.get('recommendations').get('total'):
                new_data['total_recommendations'] = (
                    data.get('recommendations').get('total')
                )
            else:
                new_data['total_recommendations'] = None
        else:
            new_data['total_recommendations'] = None

        if data.get('achievements'):
            if data.get('achievements').get('total'):
                new_data['total_achievements'] = (
                    data.get('achievements').get('total')
                )
            else:
                new_data['total_achievements'] = None
        else:
            new_data['total_achievements'] = None

        if data.get('release_date'):
            if data.get('release_date').get('cooming_soon') is False:
                release_date = data.get('release_date').get('date')
                try:
                    release_date = time\
                        .strptime(release_date, "%d %b, %Y")

                    new_data['release_date_ts'] = release_date
                    new_data['release_date_str'] = None
                except BaseException:
                    new_data['release_date_ts'] = None
                    new_data['release_date_str'] = release_date
            else:
                new_data['release_date_ts'] = None
                new_data['release_date_str'] = None
        else:
            new_data['release_date_ts'] = None
            new_data['release_date_str'] = None

        new_data['datetime'] = now.strftime("%Y-%m-%d %H:00:00")

        dim_data.append(new_data)

    write_file(dim_data, "hourly.game.gameDetail", now)


if __name__ == '__main__':
    get_game_detail()
